#!/usr/local/bin/zsh

local sessions=""

foreach line in $(tmux ls -F '#{session_name}'); do

	if [ ! -z ${sessions} ]; then
		sessions="${sessions} "
	fi

	sessions="${sessions}${line}"
done

if [ -z ${sessions} ]; then
	sessions="NONE"
fi

echo "${sessions}"
