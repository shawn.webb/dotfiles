#!/usr/bin/env zsh

GIT=$(which git)
os=$(uname)
os=${os:l}

cd ~

rm -f .config/fish/fish_variables
rm -f .config/kitty/kitty.conf
rm -f .config/starship.toml
rm -f .config/sakura/sakura.conf
rm -f .i3/config
rm -f .i3status.conf
rm -f .muttrc
rm -f .profile
rm -f .tmux.conf
rm -f .vimrc
rm -f .zshrc

if [ ! -d .mutt ]; then
    mkdir -p .mutt/cache/headers/../bodies
    touch .mutt/certificates
    touch .mutt/credentials
    touch .mutt/folders
    touch .mutt/aliases
fi

if [ ! -d .oh-my-zsh ]; then
    git clone -c http.sslVerify=false https://git.hardenedbsd.org/shawn.webb/oh-my-zsh.git .oh-my-zsh
fi

plugins=""
while getopts 'p:' o; do
	case "${o}" in
	p)
		if [ ! -d .oh-my-zsh/plugins/${OPTARG} ]; then
			echo "Warning: oh-my-zsh plugin ${OPTARG} not found"
		fi

		plugins="${plugins} ${OPTARG}"
		;;
	*)
		echo "USAGE: ${0} [-p plugin]"
		echo "    -p plugin        Add zsh plugin. May be specified multiple times"
		exit 1
		;;
	esac
done

sed "s,__PLUGINS__,${plugins}," ~/dotfiles/zshrc.templ > ~/dotfiles/zshrc

if [ ! -d .i3 ]; then
    mkdir -p .i3
fi

if [ ! -d .config ]; then
	mkdir -p .config
fi

[ ! -d .config/kitty ] && mkdir -p .config/kitty
[ ! -d .config/fish ] && mkdir -p .config/fish
[ ! -d .config/sakura ] && mkdir -p .config/sakura

ln -s ~/dotfiles/i3status.${os}.conf .i3status.conf
ln -s ~/dotfiles/i3config.${os} .i3/config
ln -s ~/dotfiles/.freebsd_profile .profile
ln -s ~/dotfiles/.tmux.conf .tmux.conf
ln -s ~/dotfiles/zshrc .zshrc
ln -s ~/dotfiles/muttrc .muttrc
ln -s ~/dotfiles/.vimrc .vimrc
ln -s ~/dotfiles/neomutt-gpg.rc .mutt/gpg.rc
ln -s ~/dotfiles/starship.toml .config/starship.toml
ln -s ~/dotfiles/kitty.conf .config/kitty/kitty.conf
ln -s ~/dotfiles/fish_variables .config/fish/fish_variables
ln -s ~/dotfiles/sakura.conf .config/sakura/sakura.conf

if [ ! -d tmux/logs ]; then
    mkdir -p tmux/logs
fi
